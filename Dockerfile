FROM centos:7
WORKDIR /python_api
COPY ./python-api.py .
RUN yum install -y python3
RUN pip3 install flask flask-jsonpify flask-restful
RUN chmod +x ./python-api.py
CMD ["python3", "python-api.py"]